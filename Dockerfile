FROM node
WORKDIR /opt/backend
COPY ./src/backend .

RUN npm install -g npm@7.16.0
RUN npm install -g nodemon
RUN npm install --production

CMD [ "npm", "start" ]