const fs = require('fs');
const path = require('path');
const DEVELOPMENT = 'DEVELOPMENT';
const QA = 'QA';
const PRODUCTION = 'PRODUCTION';
const TEST = 'TEST';
const fileTestUrl = path.join(__dirname, '/test.config.json');

/**
 * This method set the application in test mode
 * @param {string} tempUrlMongo This is the temp url of the mongodb
 * @return {ProcessEnv} The process.env configured object
 */
const setupTestEnvironment = (tempUrlMongo) => {
    try {
        if (fs.statSync(fileTestUrl, {throwIfNoEntry: false})) fs.rmSync(fileTestUrl);
        fs.writeFileSync(fileTestUrl, JSON.stringify({
            tempUrlMongo,
        }));
        return setup();
    } catch (e) {
        throw new Error(`Error initialization the TEST environment ${e}`);
    }
};

/**
 * This method sets the config for the database depending on the environment
 * @param {{uri,port,dbName}} tempUrlMongo The url of the mongo database in TEST environment
 * @return {ProcessEnv} void
 */
const setupDbSettings = (tempUrlMongo = null) => {
    // DB name
    if (process.env.ENVIRONMENT === DEVELOPMENT) {
        if (!process.env.DBNAME) process.env.DBNAME = 'pnud_atlas_dev';
    }
    if (process.env.ENVIRONMENT === QA) {
        if (!process.env.DBNAME) process.env.DBNAME = 'pnud_atlas_qa';
    }
    if (process.env.ENVIRONMENT === PRODUCTION) {
        if (!process.env.DBNAME) process.env.DBNAME = 'pnud_atlas_production';
    }
    if (tempUrlMongo) {
        process.env.DBHOST = process.env.IS_GITLAB_TESTING ? 'mongo' : '127.0.0.1';
        process.env.DBPORT = process.env.IS_GITLAB_TESTING ? '27017' : tempUrlMongo.port;
        process.env.DBNAME = process.env.IS_GITLAB_TESTING ? 'pnud_atlas_test' : tempUrlMongo.dbName;
    }
    // Host
    if (!process.env.DBHOST) process.env.DBHOST = 'mongo';
    // Credentials
    if (!process.env.DBUSERNAME) process.env.DBUSERNAME = '';
    if (!process.env.DBPASSWORD) process.env.DBPASSWORD = '';
    if (!process.env.DBAUTHDB) process.env.DBAUTHDB = 'admin';
    if (process.env.DBPASSWORD) process.env.DBPASSWORD = `:${process.env.DBPASSWORD}@`;
    // Port
    if (!process.env.DBPORT && process.env.DBHOST !== 'mongo') process.env.DBPORT = '27017';
    if (!process.env.DBPORT && process.env.DBHOST === 'mongo') process.env.DBPORT = '';
    if (process.env.DBPORT !== '') process.env.DBPORT = `:${process.env.DBPORT}`;
    return process.env;
};

const setup = () => {
    let tempUrlMongo = null;
    if (!process.env.ENVIRONMENT) {
        try {
            let fileTest = fs.statSync(fileTestUrl, {
                throwIfNoEntry: false,
            });
            // eslint-disable-next-line no-console
            fileTest = JSON.parse(fs.readFileSync(fileTestUrl).toString());
            tempUrlMongo = fileTest.tempUrlMongo;
            process.env.ENVIRONMENT = TEST;
        } catch (e) {
            tempUrlMongo = null;
            process.env.ENVIRONMENT = DEVELOPMENT;
        }
    }
    const possibleEnvironments = [DEVELOPMENT, QA, PRODUCTION];
    if (!possibleEnvironments.includes(process.env.ENVIRONMENT) && process.env.ENVIRONMENT !== TEST) process.env.ENVIRONMENT = DEVELOPMENT;
    try {
        if (process.env.ENVIRONMENT !== TEST) fs.rmSync(fileTestUrl);
    } catch (e) {
        // eslint-disable-next-line no-empty
    }
    return setupDbSettings(tempUrlMongo);
};

setup();

module.exports = {
    setupConfig: setup,
    setupTestEnvironment,
    DEVELOPMENT,
    QA,
    PRODUCTION,
    TEST,
};
