const Hapi = require('@hapi/hapi');
const path = require('path');
const fs = require('fs');
const port = process.env.PORT || 8080;
const server = Hapi.server({
    port: port,
});
const logger = require('../util/logger');
const mongo = require('../model/db/db');
const UUID = require('../util/data/data.util');


/**
 * Routes
 */

/**
 * This method will find recursive every file that ends with .controller.js in src/controller folder
 * @param {string[]} result This is the final result
 * @param {string} parentPath This is the path to find files
 * @return {string[]} The result object
 */
const findControllers = (result, parentPath) => {
    try {
        if (fs.statSync(parentPath).isFile()) {
            if (parentPath.endsWith('.controller.js')) result.push(parentPath);
            return result;
        }
        const filesList = fs.readdirSync(parentPath);
        for (const filesListElement of filesList) findControllers(result, path.join(parentPath, filesListElement));
    } catch (e) {
        logger.error('Error in path', parentPath, e);
        throw new Error('Error building the controllers');
    }
    return result;
};

const configControllers = () => {
    const controllers = [];
    const controllersFound = findControllers([], path.join(__dirname, '../controller'));
    controllersFound.forEach((p) => {
        p = p.replace(path.join(__dirname, '../controller'), '../controller');
        controllers.push(require(p));
    });
    for (const controller of controllers) {
        if (!controller?.setup) throw Error('Each controller must export a setup function');
        controller.setup(server);
    }
};

const start = async () => {
    try {
        logger.log('Starting server..............');
        logger.log(UUID.getUUID());
        configControllers();
        await server.start();
        const connection = await mongo.connect();
        logger.log('Server start', {
            port,
        });
        return connection.connection !== null;
    } catch (e) {
        throw new Error(e);
    }
};

const stop = async () => {
    logger.log('Stopping server..............');
    await server.stop();
    await mongo.close();
    logger.log('Server stopped');
};

module.exports = {
    start,
    stop,
};
