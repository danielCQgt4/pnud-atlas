const logger = require('./logger');
const security = require('../lib/security/security');
const dataUtil = require('./data/data.util');

/**
 * The universal handler use in this project to handle error and security
 * @param {any} request
 * @param {any} h
 * @param {Function} processor
 * @param {Boolean} freeToPass
 * @return {Promise<*|*>}
 */
const handler = async (request, h, processor, freeToPass = false) => {
    try {
        let response;
        let jwt = request?.headers?.authorization;
        if (jwt) {
            jwt = jwt.replace('Bearer ', '');
            const infoJwt = await security.isSessionValid(jwt);
            if (!infoJwt.validToken && !freeToPass) {
                throw new dataUtil.NotAccessError('Tu sesion ha expirado o no has iniado sesion');
            }
            freeToPass = true;
            if (infoJwt.user) {
                request.session = {
                    userId: infoJwt.user._a, email: infoJwt.user._b, expiration: infoJwt.user._c,
                };
            }
        }
        if (freeToPass) response = await processor(request, h);
        if (!freeToPass && !jwt) {
            throw new dataUtil.NotAccessError('Tu sesion ha expirado o no has iniado sesion');
        }
        if (!response) {
            throw Object.create({message: 'Not response detected', code: 500});
        }
        return response;
    } catch (errRes) {
        await logger.error(errRes, {from: processor});
        // eslint-disable-next-line no-ex-assign
        if (!errRes) errRes = {code: 400, message: 'No se pudo procesar la tarea con exito'};
        let code = errRes.code;
        if (!code) code = 400;
        return h.response(errRes).code(code);
    }
};


module.exports = {handler};
