/* eslint-env es6 */
const path = require('path');
const fs = require('fs/promises');
const environmentConfig = require('../config/environment.config');
const pathLogFile = [
    `/../../logs/error_${process.env.ENVIRONMENT}.log`,
    path.join(__dirname, `/../../logs/error_${process.env.ENVIRONMENT}.log`),
];

/**
 * This function prints the information in the console indicating the output type and env
 * @param {string} c: This is the color of the output
 * @param {string} str: This is the prefix information in the output
 * @param {IArguments} args:  This is the information to show
 * @return {Object} The string printed
 */
function printer(c, str, ...args) {
    for (const arg of args) {
        for (const e of arg) {
            if (['undefined', 'boolean', 'bigint', 'symbol', 'number', 'string'].includes(typeof e)) {
                str += `${e} `;
                continue;
            }
            try {
                if (typeof arg == 'object') str += `${JSON.stringify(e)} `;
            } catch (ex) {
                str += `${e} `;
            }
        }
    }
    // eslint-disable-next-line no-console
    console.log(c, str, '\x1b[0m');
    return [c, str, '\x1b[0m'];
}

/**
 * This method check if there if server valid path
 * @param {string} _path The path to check
 * @return {Promise<boolean>} If the path exists
 */
async function pathExists(_path) {
    try {
        return await fs.stat(path.join(__dirname, _path)) !== null;
    } catch (e) {
        if (e.code === 'ENOENT') return false;
    }
    return false;
}

module.exports = {
    pathLogFile,
    log: function () {
        if (!process.env.ENVIRONMENT) return;
        // eslint-disable-next-line prefer-rest-params
        return printer('\x1b[0m', `[${process.env.ENVIRONMENT}-INFO] `, arguments);
    },
    debug: function () {
        if (process.env.ENVIRONMENT && process.env.ENVIRONMENT !== environmentConfig.DEVELOPMENT) return null;
        // eslint-disable-next-line prefer-rest-params
        return printer('\x1b[4m', `[${process.env.ENVIRONMENT}-DEBUG] `, arguments);
    },
    error: async function () {
        if (!process.env.ENVIRONMENT) return;
        // eslint-disable-next-line prefer-rest-params
        const str = printer('\x1b[31m', `[${process.env.ENVIRONMENT}-ERROR] `, arguments);
        if (!(await pathExists('/../../logs'))) return;
        const date = new Date();
        const errLog = `${date.toISOString()} -  ${str} + ${String.fromCharCode(10)}`;
        if (!(await pathExists(pathLogFile[0]))) await fs.writeFile(pathLogFile[1], errLog);
        await fs.appendFile(pathLogFile[1], errLog);
        return str;
    },
};
