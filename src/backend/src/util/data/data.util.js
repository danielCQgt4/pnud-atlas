let timeToChange = 20;
let currentDate;
let strNumberDate;
const bcrypt = require('bcrypt');
const BCRYPT_SALT = 12;


/**
 * ResponseError model
 */
class
// eslint-disable-next-line no-unused-vars
ResponseError {
    /**
     * User error constructor
     * @param {string} _message
     * @param {string} _action
     * @param {any} _result
     */
    constructor(_message, _action, _result) {
        this.message = _message;
        this.action = _action;
        this.result = _result;
    }
}

/**
 * NotAccessError model
 */
class
// eslint-disable-next-line no-unused-vars
NotAccessError {
    /**
     * User error constructor
     * @param {string} _message
     */
    constructor(_message) {
        this.message = _message;
        this.code = 401;
    }
}

/**
 * NotAccessError model
 */
class
// eslint-disable-next-line no-unused-vars
NotFoundError {
    /**
     * User error constructor
     * @param {string} _message
     */
    constructor(_message) {
        this.message = _message;
        this.code = 404;
    }
}

const getUUID = () => {
    changeTime();
    let uuid = '';
    let code;
    for (let i = 0; i < strNumberDate.length; i++) {
        code = Math.floor(i % 7 === 0 ? (Math.random() * (90 - 65)) + 65 : (Math.random() * (122 - 97)) + 97);
        uuid += `${strNumberDate[i]}${i % 3 === 0 ? code : String.fromCharCode(code)}`;
        if ((i + 1) < strNumberDate.length) uuid += `${strNumberDate[i + 1]}`;
    }
    return uuid;
};

const changeTime = () => {
    if (timeToChange < 20) return timeToChange++;
    currentDate = new Date(new Date().toUTCString());
    strNumberDate = `${currentDate.getFullYear()}${currentDate.getMonth()}${currentDate.getDay()}` +
        `${currentDate.getHours()}${currentDate.getMinutes()}${currentDate.getSeconds()}${currentDate.getMilliseconds()}`;
    timeToChange++;
    if (timeToChange > 20) timeToChange = 0;
};

/**
 * This method will be use to validate strings
 * @param {String} string The string to validate
 * @return {string} the length of the string
 */
const stringValidator = (string) => {
    return string?.trim().length;
};

/**
 * This method will be use to validate strings
 * @param {String[]} strings The string to validate
 * @param {String[]} messages The string messages in case error
 * @return A {boolean} a boolean if string if valid
 * @return {string|null} a message if there is an string error, otherwise will return null
 */
const stringsValidator = (strings, messages) => {
    if (strings?.length === 0 || messages?.length === 0 || (messages.length !== strings.length)) throw new Error('Strings validator not well formed');
    for (let i = 0; i < strings.length; i++) if (!stringValidator(strings[i])) return messages[i];
    return null;
};

/**
 * Method to validate an email
 * @param {string} email
 * @return {boolean} If the email is ok
 */
const emailValidator = (email) => {
    if (!stringValidator(email)) return false;
    try {
        let [username, domain] = email.split('@');
        if (!stringValidator(username) || !stringValidator(domain)) return false;
        domain = domain.split('.');
        if (domain.length < 2 || !stringValidator(domain[0])) return false;
        for (let i = 1; i < domain.length; i++) if (!stringValidator(domain[i]) && domain[i].length <= 1) return false;
    } catch (e) {
        return false;
    }
    return true;
};

/**
 * This method takes a string then converts it to hash encryption
 * @param {string} _text The value to encrypt
 * @return {string} the hash value
 */
const encryptDataHash = async (_text) => {
    try {
        return await bcrypt.hash(_text, BCRYPT_SALT);
    } catch (e) {
        return null;
    }
};

/**
 * The method will compare the hash value against the plain text
 * @param {string} _text The first string to compare
 * @param {string} hash The second string to compare
 * @return {Promise<boolean>}
 */
const hashCompare = async (_text, hash) => {
    try {
        return await bcrypt.compare(_text, hash);
    } catch (e) {
        return false;
    }
};

const getTimeUTC = () => {
    return new Date(new Date().toUTCString());
};

module.exports = {
    hashCompare,
    encryptDataHash,
    emailValidator,
    stringsValidator,
    stringValidator,
    getUUID,
    getTimeUTC,
    ResponseError,
    NotAccessError,
    NotFoundError,
};
