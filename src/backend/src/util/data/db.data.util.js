/**
 *
 * @param {any} dbActionResponse
 * @return {boolean}
 */
const validateActionDone = (dbActionResponse) => {
    if (!dbActionResponse) return false;
    return !(dbActionResponse.errors || (Array.isArray(dbActionResponse.errors) && dbActionResponse.errors.length));
};

module.exports = {validateActionDone};
