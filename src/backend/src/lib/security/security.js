const dataUtil = require('../../util/data/data.util');
const userDB = require('../../model/security/user.db');
const jwtWebToken = require('jsonwebtoken');
const DbUtil = require('../../util/data/db.data.util');
const logger = require('../../util/logger');

/**
 *
 * @param {UserModel} user
 * @return {Promise<{}>}
 */
const login = async (user) => {
    if (!user || !dataUtil.stringValidator(user.email) || !dataUtil.stringValidator(user.password)) return {needCredentials: true}; // NEEDS CREDENTIALS
    const _user = await userDB.getUserByEmail(user.email);
    if (!_user) return {wrongCredentials: true}; // WRONG CREDENTIALS
    const passCompared = await dataUtil.hashCompare(user.password, _user.password);
    if (!passCompared) return {wrongCredentials: true}; // WRONG CREDENTIALS
    const currentDate = dataUtil.getTimeUTC();
    const addSessionResult = await userDB.addSessionToUser(_user, {
        sessionId: dataUtil.getUUID(),
        jwt: await generateToken(_user, currentDate),
        createdAt: currentDate,
        limitMs: currentDate.getTime() + (60000 * 60),
        sessionValid: true,
    });
    if (addSessionResult?.jwt) {
        return {loginSuccess: true, jwt: addSessionResult.jwt};// Login success
    }
    return {loginSuccess: false}; // Login fail
};

/**
 * This method will create server session for an specific user
 * @param {UserModel} _user The user to create the session
 * @param {Date} currentTime The current time when login
 * @return {string} The JWT
 */
const generateToken = async (_user, currentTime) => {
    // eslint-disable-next-line no-async-promise-executor
    return new Promise(async (resolve, reject) => {
        const expiration = new Date();
        // Give 30 minute to the session
        expiration.setTime(currentTime.getTime() + (60000 * 60));
        jwtWebToken.sign({
            iat: expiration.getTime() / 1000, // Limit time in seconds
            exp: expiration.getTime() / 1000, // Limit time in seconds
            _a: _user.userId,
            _b: await dataUtil.encryptDataHash(_user.email),
            _c: expiration.getTime(),
        }, process.env.JWT_SECRET, {algorithm: 'HS384'}, (err, token) => {
            if (err) return reject(err);
            resolve(token);
        });
    });
};

const validateToken = async (jwtToValidate) => {
    return new Promise((resolve, reject) => {
        jwtWebToken.verify(jwtToValidate, process.env.JWT_SECRET, (err, isValid) => {
            // eslint-disable-next-line prefer-promise-reject-errors
            if (err) return resolve(false);
            const currentDate = dataUtil.getTimeUTC();
            const jwtTime = isValid.exp * 1000;
            if (jwtTime > currentDate.getTime()) return resolve(isValid);
            return false;
        });
    });
};

/**
 * This method validates if a jwt if valid
 * @param {string} jwt The jwt to check
 * @return {Promise<{}>}
 */
const isSessionValid = async (jwt) => {
    const userInSession = await userDB.getUserByLastJwt(jwt);
    if (!userInSession) return {validToken: false, user: userInSession};
    const validToken = await validateToken(jwt);
    if (!validToken) return {validToken: false, user: userInSession};
    if ((validToken.exp * 1000) !== userInSession.sessions[0].limitMs) return {validToken: false, user: userInSession};
    return {validToken, user: userInSession};
};

// ***************************************************************************************************************************************************************************************************************************************************
// ***************************************************************************************************************************************************************************************************************************************************
// ***************************************************************************************************************************************************************************************************************************************************

const validatePassword = (password) => {
    const error = new dataUtil.ResponseError('La contrasena no cumple con los requisitos de seguridad', 'FAILED_VALIDATION', {
        'min-length': 8,
        'min-letters-upperCase': 1,
    });
    if (!password) throw error;
    if (typeof password !== 'string') throw error;
    if (password.length < 8) throw error;
    let capLetterFound = false;
    for (let i = 0; i < password.length; i++) {
        const codeLetter = password.charCodeAt(i);
        if ((codeLetter >= 65 && codeLetter <= 90) || codeLetter === 165) {
            capLetterFound = true;
            break;
        }
    }
    if (!capLetterFound) throw error;
    return password;
};

/**
 * This method will help to create server new user
 * @param {UserModel} user The user to add
 * @return {Promise<{created: boolean}>}
 */
const createUser = async (user) => {
    user.password = validatePassword(user.password);
    user.passwordHistory = [{
        update: dataUtil.getTimeUTC(),
        password: user.password,
    }];
    const create = await userDB.createUser(user);
    if (DbUtil.validateActionDone(create)) return {created: true};
    return {created: false};
};

/**
 * This method updates the information for an specific user
 * @param {UserModel} user
 * @param {Boolean} changePassword
 * @param {Boolean} lookupUser
 * @return {Promise<{updated: boolean}|{userNotExists: boolean, updated: boolean}>}
 */
const updateUser = async (user, changePassword = false, lookupUser = true) => {
    let _user = user;
    if (lookupUser) _user = await userDB.getUserByEmail(user.email);
    logger.debug(`The user information goes from ${JSON.stringify(_user)} to ${JSON.stringify(user)}`);
    if (!_user) throw new dataUtil.ResponseError(`El usuario con el correo ${user.email} no existe`, 'FAILED', null);
    if (!changePassword) user.password = _user.password;
    user.userId = _user.userId;
    const update = await userDB.updateExistingUser(user);
    if (DbUtil.validateActionDone(update)) return {updated: true};
    return {updated: false};
};

/**
 * This method updates the user password
 * @param {UserModel} user
 * @return {Promise<{updated: boolean}|{userNotExists: boolean, updated: boolean}>}
 */
const updatePassword = async (user) => {
    if (!user || !user.password) throw new dataUtil.ResponseError('Los campos contrasena y correo son necesarios', 'FAILED', null);
    const _user = await userDB.getUserByEmail(user.email);
    if (!_user) throw new dataUtil.ResponseError(`El usuario con el correo ${user.email} no existe`, 'FAILED', null);
    if (!_user.passwordHistory) _user.passwordHistory = [];
    _user.passwordHistory.push({
        update: dataUtil.getTimeUTC(), password: _user.password,
    });
    _user.password = validatePassword(user.password);
    return updateUser(_user, true, false);
};

const getUser = async (email) => {
    try {
        return await userDB.getUserByEmail(email);
    } catch (e) {
        return false;
    }
};

const getUsers = async () => {
    try {
        return await userDB.getUsers();
    } catch (e) {
        return false;
    }
};

module.exports = {
    // Security
    login,
    isSessionValid,
    // User
    createUser,
    updateUser,
    updatePassword,
    getUser,
    getUsers,
};

