const UserLib = require('../../../lib/security/security');
const dataUtil = require('../../../util/data/data.util');

const createUser = async (request, h) => {
    const user = request.payload;
    const createResponse = await UserLib.createUser(user);
    if (createResponse.created) return h.response({}).code(201);
    return h.response({message: 'El usuario no pudo ser creado', created: false}).code(400);
};

const updateUser = async (request, h) => {
    const {payload, params} = request;
    let response;
    if (!['password', 'information'].includes(params.type)) return h.response({message: 'El tipo puede ser \'password\' o \'information\''}).code(400);
    if (params.type === 'password') response = await UserLib.updatePassword(payload);
    if (params.type === 'information') response = await UserLib.updateUser(payload, false, true);
    if (response.updated) return h.response().code(204);
    return h.response({
        message: (params.type !== 'password') ? 'El usuario no pudo ser modificado' : 'La contrasena no pudo ser modificada',
    }).code(400);
};

const listUsers = async (request, h) => {
    if (request.params.getUser) {
        const user = await UserLib.getUser(request.params.getUser);
        if (!user) throw new dataUtil.NotFoundError(`El usuario con el correo ${request.params.getUser} no existe`);
        return h.response(user).code(200);
    }
    const users = await UserLib.getUsers();
    if (!users) throw new dataUtil.ResponseError('Error al obtener los usuariot', 'GET_FAIL', null);
    return h.response(users).code(200);
};

module.exports = {
    createUser,
    updateUser,
    listUsers,
};
