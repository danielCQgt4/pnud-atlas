const security = require('../../../lib/security/security');
const logger = require('../../../util/logger');

const login = async (request, h) => {
    const response = {message: '', loginSuccess: false};
    const user = request.payload;
    const loginResult = await security.login(user);
    response.loginSuccess = loginResult.loginSuccess || false;
    if (loginResult.needCredentials) response.message = 'Se necesita de un correo y una contrasena';
    if (loginResult.wrongCredentials) response.message = 'Las credenciales son incorrectas';
    if (!loginResult.loginSuccess && !loginResult.needCredentials && !loginResult.wrongCredentials) response.message = 'No se pudo iniciar session';
    if (response.message !== '') return h.response(response).code(401);
    if (loginResult.loginSuccess) {
        response.message = 'Sesion iniciada correctamente';
        response.jwt = loginResult.jwt;
        logger.debug('Session iniciada', {user, jwt: loginResult.jwt});
    }
    return h.response(response).code(200);
};

const validateSession = async (request, h) => {
    let jwt = request?.headers?.authorization;
    if (!jwt) return h.response({valid: false}).code(401);
    jwt = jwt.replace('Bearer ', '');
    const isValid = await security.isSessionValid(jwt);
    return h.response({valid: !!isValid.validToken}).code(isValid.validToken ? 200 : 401);
};

module.exports = {
    login,
    validateSession,
};
