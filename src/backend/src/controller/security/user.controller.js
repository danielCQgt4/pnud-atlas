const UniversalHandler = require('../../util/universalHandler.util');
const UserHandler = require('./handler/user.handler');

/**
 * @param {internals.Server} server:  This is the server context
 */
const setup = (server) => {
    server.route({
        method: 'POST',
        path: '/security/user/create',
        handler: (request, h) => UniversalHandler.handler(request, h, UserHandler.createUser),
    });
    server.route({
        method: 'POST',
        path: '/security/user/update/{type}',
        handler: (request, h) => UniversalHandler.handler(request, h, UserHandler.updateUser),
    });
    server.route({
        method: 'GET',
        path: '/security/user/get',
        handler: (request, h) => UniversalHandler.handler(request, h, UserHandler.listUsers),
    });
    server.route({
        method: 'GET',
        path: '/security/user/get/{getUser}',
        handler: (request, h) => UniversalHandler.handler(request, h, UserHandler.listUsers),
    });
};

module.exports = {setup};
