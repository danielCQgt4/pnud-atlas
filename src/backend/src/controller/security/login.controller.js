const LoginHandler = require('./handler/login.handler');
const UniversalHandler = require('../../util/universalHandler.util');

/**
 * @param {internals.Server} server:  This is the server context
 */
const setup = (server) => {
    server.route({
        method: 'POST',
        path: '/security/login',
        handler: (request, h) => UniversalHandler.handler(request, h, LoginHandler.login, true),
    });
    server.route({
        method: 'GET',
        path: '/security/session/check',
        handler: async (request, h) => {
            return await UniversalHandler.handler(request, h, LoginHandler.validateSession, true);
        },
    });
};

module.exports = {setup};
