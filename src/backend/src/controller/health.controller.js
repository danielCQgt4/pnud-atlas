require('../config/environment.config');

/**
 * @param {internals.Server} server:  This is the server context
 */
const healthy = (server) => {
    server.route({
        method: 'GET',
        path: '/health',
        handler: () => 'Good',
    });
    server.route({
        method: 'GET',
        path: '/env',
        handler: () => process.env.ENVIRONMENT,
    });
};

module.exports = {setup: healthy};
