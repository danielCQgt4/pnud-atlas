const isValidationIssue = (resultDb) => {
    return typeof resultDb === 'string';
};

const wasUpdated = (resultDb) => {
    if (resultDb) return true;
    return false;
};

const wasInserted = (resultDb) => {

};

const wasRemoved = (resultDb) => {

};

module.exports = {
    wasInserted,
    wasUpdated,
    wasRemoved,
    isValidationIssue,
};
