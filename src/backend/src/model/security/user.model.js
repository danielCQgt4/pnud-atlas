const mongoose = require('mongoose');

const userMessages = {
    ACTION_NOT_PERMITTED: 'Esta accion no esta permitida',
    NOT_NAME: 'Debes completar el nombre del usuario',
    NOT_LASTNAME: 'Debes incluir los apellidos del usuario',
    NOT_EMAIL: 'Debes ingresar un correo valido como ejemplo@ejemplo',
    REPEATED_EMAIL: 'es un correo que ya se encuentra en el sistema',
    NOT_PASSWORD: 'Debes ingresar una contrasena',
    NOT_ID_USER_TO_FOUND: 'Id de usuario no proporsionado',
};

/**
 * User model
 */
class
// eslint-disable-next-line no-unused-vars
UserModel {
    /**
     * User constructor
     */
    constructor() {
        this.userId = '';
        this.name = '';
        this.lastName = '';
        this.email = '';
        this.password = '';
        this.lastJwt = '';
        this.createAt = null;
        this.passwordHistory = [];
        this.sessions = [
            {
                sessionId: '',
                jwt: '',
                createdAt: '',
                limitMs: 0.0,
                sessionValid: true,
            },
        ];
    }
}

const userSchema = new mongoose.Schema({
    userId: {
        type: String,
        index: {
            unique: true,
        },
    },
    email: {
        type: String,
        index: {
            unique: true,
        },
    },
    lastJwt: {
        type: String,
        index: {
            unique: true,
        },
    },
}, {
    strict: false,
});

const sessionHistory = new mongoose.Schema({
    userId: {
        type: String,
    },
    session: {
        sessionId: String,
        jwt: String,
        createdAt: Date,
        limitMs: Number,
        sessionValid: Boolean,
    },
});

module.exports = {
    userMessages,
    userDBModel: mongoose.model('User', userSchema, 'user'),
    sessionHistoryDBModel: mongoose.model('SessionHistory', sessionHistory, 'sessions_history'),
    model: UserModel,
};
