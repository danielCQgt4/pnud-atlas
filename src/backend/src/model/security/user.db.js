const User = require('./user.model');
const dataUtil = require('../../util/data/data.util');
const dataDbUtil = require('../../util/data/db.data.util');
const logger = require('../../util/logger');

/**
 * This method is use to validate the user information
 * @param {String} validatorType {String} This is the way this validator is gonna validate the information,
 * @param {UserModel} user This is the user itself
 * @param {boolean} cipherPassword This works to encrypt the password
 * @return {string|null} if there is an error in the data validation
 */
const userValidator = async (validatorType, user, cipherPassword = true) => {
    const {CREATE, UPDATE, ADD_SESSION} = {CREATE: 'create', UPDATE: 'update', ADD_SESSION: 'addSession'};
    if (![CREATE, UPDATE, ADD_SESSION].includes(validatorType)) return User.userMessages.ACTION_NOT_PERMITTED;
    if (validatorType === ADD_SESSION || validatorType === UPDATE) {
        if (!dataUtil.stringValidator(user.userId)) return User.userMessages.NOT_ID_USER_TO_FOUND;
        if (validatorType === ADD_SESSION) return null;
    }
    const listStringDataGeneral = dataUtil.stringsValidator([user.name, user.lastName, user.email],
        [User.userMessages.NOT_NAME, User.userMessages.NOT_LASTNAME, User.userMessages.NOT_EMAIL]);
    if (listStringDataGeneral) return listStringDataGeneral;
    if (validatorType === CREATE) {
        user.userId = dataUtil.getUUID();
        user.lastJwt = dataUtil.getUUID();
        if (!dataUtil.stringValidator(user.password)) return User.userMessages.NOT_PASSWORD;
        if (!dataUtil.emailValidator(user.email)) return User.userMessages.NOT_EMAIL;
        user.email = user.email.trim();
        const userExistingByEmail = await getUserByEmail(user.email);
        if (userExistingByEmail) return `${user.email} ${User.userMessages.REPEATED_EMAIL}`;
    }
    if (validatorType === CREATE || validatorType === UPDATE) user.password = cipherPassword ? await dataUtil.encryptDataHash(user.password) : user.password;
    return null;
};

/**
 * This method add the user information into the db
 * @param {UserModel} user The user object to insert into the db
 * @return {Promise<EnforceDocument<T, any>>}
 */
const createUser = async (user) => {
    const _val = await userValidator('create', user);
    const failUserResponse = JSON.parse(JSON.stringify(user));
    delete failUserResponse.password;
    delete failUserResponse.userId;
    delete failUserResponse.lastJwt;
    if (_val) throw new dataUtil.ResponseError(_val, 'FAILED', failUserResponse);
    try {
        user.createAt = dataUtil.getTimeUTC();
        return User.userDBModel.create(user);
    } catch (e) {
        throw new dataUtil.ResponseError('El usuario no pudo ser creado', 'NOT_CREATED', null);
    }
};

/**
 * This method update the user information
 * @param {UserModel} user The user object to update
 * @param {boolean} newSession This flag work to say if the user will keep the same sessions
 * @param {boolean} lookupUser This flag is use to indicate if it's necessary to lookup the user from the db to restore some data in user param
 * @return {Promise<{}|Query<Document<any, any> | null, Document<any, any>, {}>>}
 */
const updateExistingUser = async (user, newSession = false, lookupUser = true) => {
    if (lookupUser) { // If lookup if disable, this means that somewhere the user was get from the db
        const _val = await userValidator('update', user, !newSession);
        if (_val) throw new dataUtil.ResponseError(_val, 'FAILED', user);
    }
    try {
        let preUserUpdated = user;
        if (lookupUser) preUserUpdated = await getUserByEmail(user.email);
        if (!preUserUpdated) return {};
        if (!newSession) {
            user.lastJwt = preUserUpdated.lastJwt;
            user.sessions = preUserUpdated.sessions;
        }
        user.userId = preUserUpdated.userId;
        const sessionToHistory = [];
        let needMove;
        if (!user.sessions?.length) user.sessions = [];
        const needMoveSessionToHistory = user.sessions.length > 20;
        user.sessions = user.sessions.filter((session) => {
            session.sessionValid = session.jwt === user.lastJwt;
            needMove = needMoveSessionToHistory && !session.sessionValid;
            if (needMove) sessionToHistory.push({userId: user.userId, session});
            return !needMove;
        });
        if (needMoveSessionToHistory) await addSessionsToHistory(sessionToHistory).then(() => logger.debug('Moving sessions to history', sessionToHistory));
        return User.userDBModel.findOneAndReplace(
            {userId: user.userId},
            user,
        );
    } catch (e) {
        await logger.error('Error when trying to update an user ', user, ' error:', e);
        return {
            errors: [
                e.toString(),
                'El usuario no se actualizo',
            ],
        };
    }
};

/**
 * This method will work to create new session for a user
 * @param {UserModel} user The user to create the session
 * @param {UserModel.sessions} session The session to assign to user
 * @return {Promise<{jwt}|{}>}
 */
const addSessionToUser = async (user, session) => {
    const _val = await userValidator('addSession', user);
    if (_val) throw new dataUtil.ResponseError(_val, 'FAILED', 'La sesion no pudo ser creada');
    try {
        user = await getUserByEmail(user.email);
        if (!user.sessions) user.sessions = [];
        user.sessions.push(session);
        user.lastJwt = session.jwt;
        const updateUser = await updateExistingUser(user, true, false);
        if (dataDbUtil.validateActionDone(updateUser)) return {jwt: null};
        return {
            jwt: user.lastJwt,
        };
    } catch (e) {
        return {
            jwt: null,
        };
    }
};

const addSessionsToHistory = async (sessions) => {
    await User.sessionHistoryDBModel.insertMany(sessions);
    return true;
};

/**
 * Get an user by its email
 * @param {string} email User's email
 * @return {Promise<UserModel>} A user object
 */
const getUserByEmail = async (email) => {
    try {
        return User.userDBModel.findOne({email}).lean();
    } catch (e) {
        return null;
    }
};

/**
 * Get an user by the last jwt
 * @param {string} lastJwt This is the jwt assign to a user, and get all of the information
 * @return {UserModel} A user object
 */
const getUserByLastJwt = async (lastJwt) => {
    try {
        return User.userDBModel.findOne({
            lastJwt,
        }, {
            _id: 0,
            userId: 1, name: 1, lastJwt: 1, lastName: 1, email: 1, sessions: {
                $elemMatch: {
                    jwt: lastJwt,
                },
            },
        }).lean();
    } catch (e) {
        return null;
    }
};

const getUsers = async () => {
    try {
        return User.userDBModel.find({}, {
            _id: 0,
            userId: 1, name: 1, lastJwt: 1, lastName: 1, email: 1, sessions: 1,
        }).lean();
    } catch (e) {
        return null;
    }
};

module.exports = {
    createUser,
    updateExistingUser,
    addSessionToUser,
    getUserByEmail,
    getUserByLastJwt,
    getUsers,
};
