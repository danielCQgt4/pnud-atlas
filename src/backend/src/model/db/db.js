const mongoose = require('mongoose');
const logger = require('../../util/logger');
require('../../config/environment.config');

let connection = null;
let strConnection = null;

/**
 * This method calculates the url of the database that needs to be use
 * @param {string} urlMongo This is the url mongo of the TEST enviroment
 * @return {string} The url connection to mongo db
 */
const setUrlConnection = () => {
    const credentials = `${process.env.DBUSERNAME}${process.env.DBPASSWORD}`;
    strConnection = `mongodb://${credentials}${process.env.DBHOST}${process.env.DBPORT}/${process.env.DBNAME}?`;
    if (credentials !== '') strConnection += `authSource=${process.env.DBAUTHDB}&`;
    if (process.env.DBOPTIONS && process.env.DBOPTIONS !== '') strConnection += `${process.env.DBOPTIONS}`;
    return strConnection;
};

const connect = async () => {
    if (connection && strConnection) return {connection};
    strConnection = setUrlConnection();
    logger.log(`Trying to connect to db on ${strConnection}`);
    try {
        connection = await mongoose.connect(strConnection, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
        });
        const events = [
            {e: 'connected', type: 'log'},
            {e: 'disconnected', type: 'error'},
            {e: 'error', type: 'error'},
            {e: 'reconnected', type: 'log'},
            {e: 'connecting', type: 'log'},
        ];
        events.forEach((event) => {
            mongoose.connection.on(event, () => {
                logger[event.type](`${event.e} to db on ${strConnection}`);
            });
        });
    } catch (e) {
        throw Error(e);
    }
    return {connection};
};

const close = async () => {
    logger.log('Disconnecting from mongodb');
    if (connection?.connection) await connection.connection.close();
    return true;
};

module.exports = {
    connect,
    close,
};
