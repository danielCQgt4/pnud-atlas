const environmentConfig = require('../../../src/config/environment.config');
const real = process.env;
const fs = require('fs');
const path = require('path');
const logger = require('../../../src/util/logger');

const testLogging = async () => {
    const envs = [environmentConfig.DEVELOPMENT, environmentConfig.QA, environmentConfig.PRODUCTION, environmentConfig.TEST];
    for (const env of envs) {
        process.env.ENVIRONMENT = env;
        logger.pathLogFile[0] = `../../../logs/error_TEST.log`;
        logger.pathLogFile[1] = path.join(__dirname, `../../../logs/error_TEST.log`);
        fs.rmSync(path.join(__dirname, `../../../logs/error_${process.env.ENVIRONMENT}.log`), { force: true });
        logger.debug(`${process.env.ENVIRONMENT} - Only Development should not be output`);
        logger.log(`${process.env.ENVIRONMENT} - Should be output`);
        await logger.error(`${process.env.ENVIRONMENT} - Should be output`);
    }
};

describe('Testing environments', () => {
    test('Logging', async () => {
        await testLogging();
    });
    test('Test DEVELOPMENT environment', () => {
        process.env.ENVIRONMENT = environmentConfig.DEVELOPMENT;
        delete process.env.DBNAME;
        delete process.env.DBHOST;
        delete process.env.DBUSERNAME;
        delete process.env.DBPASSWORD;
        delete process.env.DBAUTHDB;
        delete process.env.DBPORT;
        const { DBNAME, DBHOST, DBUSERNAME, DBPASSWORD, DBAUTHDB, DBPORT } = environmentConfig.setupConfig();
        expect(DBNAME).toBe('pnud_atlas_dev');
        expect(DBHOST).toBe('mongo');
        expect(DBUSERNAME).toBe('');
        expect(DBPASSWORD).toBe('');
        expect(DBAUTHDB).toBe('admin');
        expect(DBPORT).toBe('');
    });
    test('Test QA environment', () => {
        process.env.ENVIRONMENT = environmentConfig.QA;
        delete process.env.DBNAME;
        delete process.env.DBHOST;
        delete process.env.DBUSERNAME;
        delete process.env.DBPASSWORD;
        delete process.env.DBAUTHDB;
        delete process.env.DBPORT;
        const { DBNAME, DBHOST, DBUSERNAME, DBPASSWORD, DBAUTHDB, DBPORT } = environmentConfig.setupConfig();
        expect(DBNAME).toBe('pnud_atlas_qa');
        expect(DBHOST).toBe('mongo');
        expect(DBUSERNAME).toBe('');
        expect(DBPASSWORD).toBe('');
        expect(DBAUTHDB).toBe('admin');
        expect(DBPORT).toBe('');
    });
    test('Test PRODUCTION environment', () => {
        process.env.ENVIRONMENT = environmentConfig.PRODUCTION;
        delete process.env.DBNAME;
        delete process.env.DBAUTHDB;
        delete process.env.DBPORT;
        process.env.DBHOST = 'pnud_atlas-db.com';
        process.env.DBUSERNAME = 'pnud_atlas-user';
        process.env.DBPASSWORD = 'pnud_atlas-pass';
        const { DBNAME, DBHOST, DBUSERNAME, DBPASSWORD, DBAUTHDB, DBPORT } = environmentConfig.setupConfig();
        expect(DBNAME).toBe('pnud_atlas_production');
        expect(DBHOST).toBe('pnud_atlas-db.com');
        expect(DBUSERNAME).toBe('pnud_atlas-user');
        expect(DBPASSWORD).toBe(':pnud_atlas-pass@');
        expect(DBAUTHDB).toBe('admin');
        expect(DBPORT).toBe(':27017');
    });
    test('Test TEST environment', () => {
        delete process.env.DBNAME;
        delete process.env.ENVIRONMENT;
        delete process.env.DBHOST;
        delete process.env.DBUSERNAME;
        delete process.env.DBPASSWORD;
        delete process.env.DBAUTHDB;
        delete process.env.DBPORT;
        const calculated = environmentConfig.setupTestEnvironment('mongodb://mongo:27017/pnud_atlas_test?');
        if (process.env.IS_GITLAB_TESTING) {
            expect(calculated.DBNAME).toBe('pnud_atlas_test');
            expect(calculated.DBHOST).toBe('mongo');
        } else {
            expect(calculated.DBNAME).toBe(real.DBNAME);
            expect(calculated.DBHOST).toBe(real.DBHOST);
        }
        expect(calculated.DBUSERNAME).toBe(real.DBUSERNAME);
        expect(calculated.DBPASSWORD).toBe(real.DBPASSWORD);
        expect(calculated.DBAUTHDB).toBe(real.DBAUTHDB);
        expect(calculated.DBPORT).toBe(real.DBPORT);
    });
});
