const got = require('got');

test('Check: Server running', async () => {
    try {
        let request = await got('http://localhost:8080/health');
        expect(request.statusCode).toBe(200);
        request = await got('http://localhost:8080/env');
        expect(request.body).toBe('TEST');
    } catch (e) {
        expect(false).toBeTruthy();
    }
});
