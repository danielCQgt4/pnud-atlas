const got = require('got');
const localhost = 'http://localhost:8080';
const logger = require('../../../../src/util/logger');
const SecurityLib = require('../../../../src/lib/security/security');

/**
 * This function serve to create the user
 * @return {Promise<void>}
 */
async function createUserDef() {
    await SecurityLib.createUser({
        name: 'Master',
        lastName: 'master',
        email: 'master@pnud-atlas.com',
        password: '12345678Abc',
    });
}

let jwt;

describe('User management', () => {
    beforeAll(async () => {
        // if (jwt) return;
        const userRetrieved = await SecurityLib.login({
            email: 'master@pnud-atlas.com',
            password: '12345678Abc',
        });
        if (!userRetrieved) {
            await createUserDef();
        }
        try {
            const options = {
                method: 'POST',
                url: `${localhost}/security/login`,
                json: {
                    email: 'master@pnud-atlas.com',
                    password: '12345678Abc',
                },
                responseType: 'json',
            };
            const request = await got(options);
            expect(request.statusCode).toBe(200);
            jwt = request.body.jwt;
        } catch (e) {
            throw new Error('Error when was login');
        }
    });

    test('SUCCESS: User create', async () => {
        try {
            const user = require('../data/user.json');
            const options = {
                method: 'POST',
                headers: {
                    authorization: `Bearer ${jwt}`,
                },
                url: `${localhost}/security/user/create`,
                json: user,
                responseType: 'json',
            };
            const request = await got(options);
            logger.log(`Request status: ${request.statusCode}`);
            expect(request.statusCode).toBe(201);
        } catch (e) {
            throw new Error(e);
        }
    });

    test('FAIL: User create', async () => {
        const user = require('../data/user.json');
        const keys = Object.keys(user);
        for (const key of keys) {
            const tempUser = JSON.parse(JSON.stringify(user));
            try {
                delete tempUser[key];
                const options = {
                    method: 'POST',
                    headers: {
                        authorization: `Bearer ${jwt}`,
                    },
                    url: `${localhost}/security/user/create`,
                    json: tempUser,
                    responseType: 'json',
                };
                await got(options);
                throw Object.create({message: 'This sentence is never being executed'});
            } catch (error) {
                logger.log(`Request status: ${error.response.statusCode}`);
                expect(error.response.statusCode).toBe(400);
                expect(error.response.action).toBe('FAILED');
                expect(error.result).toBeTruthy();
                const subKeys = Object.keys(tempUser);
                for (const subKey of subKeys) {
                    if (tempUser[subKey] && subKey !== 'password') expect(error.response.body.result[subKey]).toBe(tempUser[subKey]);
                }
            }
        }
    });
});
