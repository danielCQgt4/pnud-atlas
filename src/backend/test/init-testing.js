/* eslint-disable no-console */
const app = require('../app');
const {MongoMemoryServer} = require('mongodb-memory-server');
const jest = require('jest');

const start = async () => {
    const mongodb = new MongoMemoryServer();
    const uri = await mongodb.getUri();
    const port = await mongodb.getPort();
    const dbPath = await mongodb.getDbPath();
    const dbName = await mongodb.getDbName();
    try {
        console.log({
            uri,
            port,
            dbPath,
            dbName,
        });
        const started = await app.startTesting({
            uri,
            port,
            dbPath,
            dbName,
        });
        return started;
    } catch (e) {
        console.error(`Error when server was starting ${e}`);
        console.error(e);
        throw e;
    }
};

start().then((r) => {
    console.log('Starting the testing');
    const options = {
        projects: [__dirname],
        silent: true,
    };
    jest
        .runCLI(options, options.projects)
        .then(async (success) => {
            await app.stopTesting();
            process.exit(0);
        });
}).catch((e) => {
    console.error(e);
});
