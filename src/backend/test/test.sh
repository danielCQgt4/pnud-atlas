#!bin/bash

if [ "$IS_GITLAB_TESTING" != "true" ]; then
    sudo fuser -k 8080/tcp
    npm run start-test-server &
else
    node ./test/init-testing.js
fi
# npm run test-jest
