const configServer = require('./src/config/server.config');
const configEnvironments = require('./src/config/environment.config');

const startNormal = setTimeout(() => {
    configServer.start();
}, 5000);

const startTesting = async (tempUrlMongo) => {
    clearTimeout(startNormal);
    if (process.env.IS_GITLAB_TESTING) {
        tempUrlMongo = 'mongodb://mongo:27017/pnud_atlas_test?';
    }
    delete process.env.ENVIRONMENT;
    process.env.PORT = 8888;
    await configServer.stop();
    configEnvironments.setupTestEnvironment(tempUrlMongo);
    return await configServer.start();
};

const stopTesting = async () => {
    await configServer.stop();
};

module.exports = { startTesting, stopTesting };
