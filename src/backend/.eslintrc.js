module.exports = {
    'env': {
        'commonjs': true,
        'es2021': true,
        'node': true,
        'jest/globals': true,
    },
    'extends': [
        'google',
        'eslint:recommended',
    ],
    'plugins': ['jest'],
    'parserOptions': {
        'ecmaVersion': 12,
    },
    'rules': {
        'semi': ['error', 'always'],
        'indent': ['error', 4],
        'no-var': 2,
        'no-console': 2,
        'no-empty': 2,
        'no-unsafe-finally': 0,
        'block-spacing': 0,
        'brace-style': 0,
        'object-curly-spacing': 0,
        'max-len': ['error', {'code': 250}],
        'space-before-function-paren': 0,
        'jest/no-disabled-tests': 'warn',
        'jest/no-focused-tests': 'error',
        'jest/no-identical-title': 'error',
        'jest/prefer-to-have-length': 'warn',
        'jest/valid-expect': 'error',
    },
};
