module.exports = {
    mongodbMemoryServerOptions: {
        binary: {
            version: '4.0.3',
            skipMD5: true,
        },
        instance: {
            dbName: 'pnud_atlas_test',
            port: 27017,
        },
        autoStart: false,
    },
};
