#!bin/bash

case "$1" in
    "DEVELOPMENT") 
        env="DEVELOPMENT" 
        fileConfig=$PWD/config/docker-compose-dev.yml
    ;;
    "QA") 
        env="QA"
        fileConfig=$PWD/config/docker-compose-qa.yml
    ;;
    "PRODUCTION") 
        env="PRODUCTION"
        fileConfig=$PWD/config/docker-compose-prod.yml
    ;;
esac

if [[ "$1" == "" ]]; then
    echo "Choose an environment to run backend as:"
    echo "-     (1) DEVELOPMENT"
    echo "-     (2) QA"
    echo "-     (3) PRODUCTION"
    echo ""
    read env
fi

case "$env" in
    "1") 
        env="DEVELOPMENT" 
        fileConfig=$PWD/config/docker-compose-dev.yml
    ;;
    "2") 
        env="QA"
        fileConfig=$PWD/config/docker-compose-qa.yml
    ;;
    "3") 
        env="PRODUCTION"
        fileConfig=$PWD/config/docker-compose-prod.yml
    ;;
esac

echo "Starting $env mode"
app=$(sudo docker ps)
echo $app

if [[ "$app" == *"config_pnud_atlas"* ]]; then
    case "$2" in
        "stop") option="1"
        ;;
        "restart") option="2"
        ;;
    esac
    if [[ "$option" == "" ]]; then
        echo "It looks like the container its already running. Would you like to:"
        echo "-     (1) Stop"
        echo "-     (2) Restart"
        read option
    fi
    if [[ "$option" == "1" || "$option" == "2" ]]; then
        sudo docker-compose -f$fileConfig down
        if [[ "$option" == "2" ]]; then
            sudo docker-compose -f$fileConfig up
        fi
    fi
else
    sudo docker-compose -f$fileConfig down
    sudo docker-compose -f$fileConfig up
fi
